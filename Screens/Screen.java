package KeraaOmenoitaPeli.screens;

import java.awt.event.KeyEvent;
import java.io.Serializable;
import asciiPanel.AsciiPanel;
//interface n�yt�ille
public interface Screen {
	public void displayOutput(AsciiPanel terminal);
	
	public Screen respondToUserInput(KeyEvent key);
}
