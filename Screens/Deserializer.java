package KeraaOmenoitaPeli.screens;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import KeraaOmenoitaPeli.World;
import KeraaOmenoitaPeli.Creature;

/**
 * Luokka joka lukee tiedostoista aiemmin tallennetun pelin
 */

public class Deserializer {
	
	/**
	 * metodi, joka lukee tiedostosta aiemmin tallennetun pelin maailman, mik�li sellaista ei ole, palauttaa tyhj�n
	 * @return world, maailmaolio
	 */
	
	public World deserializeWorld(){
		 
		World world;
		
		   try{
	 
			   FileInputStream fin = new FileInputStream("world.ser");
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   world = (World) ois.readObject();
			   ois.close();
	 
			   return world;
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	   } 
	
	/**
	 * metodi, joka lukee tiedostosta aiemmin tallennetun pelin pelaajan, mik�li sellaista ei ole, palauttaa tyhj�n
	 * @return
	 */
	
	public Creature deserializeCreature(){
		 
		Creature creature;
		 
		   try{
	 
			   FileInputStream fin = new FileInputStream("player.ser");
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   creature = (Creature) ois.readObject();
			   ois.close();
	 
			   return creature;
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
			   return null;
		   } 
	   } 
	
}
	
