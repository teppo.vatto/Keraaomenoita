package KeraaOmenoitaPeli.screens;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import KeraaOmenoitaPeli.World;
import KeraaOmenoitaPeli.Creature;

/**
 * Luokka joka tallentaa nykyisen pelin world ja player oliot tiedostoihin
 */

public class Serializer {
	
	/**
	 * metodi joka tallentaa annetun World-olion tiedostoon "world.ser"
	 * @param world
	 */
	public void serializeWorld(World world){
		
		
		 
		   try{
	 
			FileOutputStream fout = new FileOutputStream("world.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(world);
			oos.close();
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
		   }
	   }
	
	/**
	 * metodi joka tallentaa annetun Creature-olion tiedostoon "player.ser"
	 * @param player
	 */
	
	public void serializeCreature(Creature player){
	 
		   try{
	 
			FileOutputStream fout = new FileOutputStream("player.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(player);
			oos.close();
	 
		   }catch(Exception ex){
			   ex.printStackTrace();
		   }
	   }


}
