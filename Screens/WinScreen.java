package KeraaOmenoitaPeli.screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

public class WinScreen implements Screen {
	
	/**
	 * kirjoittaa n�yt�lle tarvittavat tiedot
	 */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.write("Kaikki omenat korissa! Loistavaa.", 1, 1);
		terminal.write("Toivottavasti "+ (char)132 +"iti suostuu leipomaan sinulle omenapiirakan!", 1, 2);
		terminal.writeCenter("Olet nyt virallisesti universumin parhaimmistoa!", 10, AsciiPanel.red);
		terminal.writeCenter("-- paina [enter] pelataksesi uudelleen --", 22);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}
