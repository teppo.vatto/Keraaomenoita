package KeraaOmenoitaPeli.screens;

import java.awt.event.KeyEvent;
import KeraaOmenoitaPeli.Creature;
import KeraaOmenoitaPeli.World;
import asciiPanel.AsciiPanel;

public class StartScreen implements Screen {
	
	//ATTRIBUUTIT (tallennettua peli� varten)
	private World world;
	private Creature player;
	
	//KONSTRUKTORI
	public StartScreen(){
		Deserializer deserializer = new Deserializer();
		world = deserializer.deserializeWorld();
		player = deserializer.deserializeCreature();
	}
	
	/**
	 * kirjoitetaan n�yt�lle tarvittavat tiedot
	 */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("Ker" +(char)132+(char)132+" Omenoita!", 11, AsciiPanel.red);
		terminal.writeCenter("Selvi"+(char)132+"tk"+(char)148+" sin"+(char)132+" ilman vatsakipuja?", 13);
		terminal.writeCenter("-- paina [V" +(char)132+"li] jatkaaksesi vanhaa peli" +(char)132+" --", 20);
		terminal.writeCenter("Tai", 21);
		terminal.writeCenter("-- paina [enter] aloittaaksesi uuden pelin --", 22);
	}
	
	
	/**
	 * Metodi, joka reagoi k�ytt�j�n painaessa enter 
	 */
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		switch (key.getKeyCode()){
		case KeyEvent.VK_ENTER: return new PlayScreen() ;
		case KeyEvent.VK_SPACE: if(world != null){
			return new PlayScreen(world, player);
			}else{return new PlayScreen();}
		}
		return this;
	}
}
