package KeraaOmenoitaPeli.screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

public class LoseScreen implements Screen {
	/**
	 * Tulostaa n�yt�lle tarvittavat tiedot
	 */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.write("Valitettavasti tuli haukattua m"+(char)132+"t"+(char)132+(char)132+" Omenaa", 1, 1);
		terminal.write("Joudut nyt keskeytt"+(char)132+"m"+(char)132+(char)132+"n poiminnat kouristuksien takia!", 1, 2);
		terminal.writeCenter("-- paina [enter] palataksesi poimintapaikalle --", 22);
	}
	
	/**
	 * odottaa pelaajan vastausta
	 */
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new PlayScreen() : this;
	}
}
