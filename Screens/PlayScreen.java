package KeraaOmenoitaPeli.screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;
import KeraaOmenoitaPeli.Creature;
import KeraaOmenoitaPeli.CreatureFactory;
import KeraaOmenoitaPeli.World;
import KeraaOmenoitaPeli.WorldBuilder;

public class PlayScreen implements Screen{
	//ATTRIBUUTIT
	private World world;
	private Creature player;
    private int screenWidth;
    private int screenHeight;
    private String saved;
    
    //KONSTRUKTORIT
    public PlayScreen(){
        screenWidth = 80;
        screenHeight = 20;
        createWorld();
        CreatureFactory creatureFactory = new CreatureFactory(world);
        createCreatures(creatureFactory);
    }
    
    public PlayScreen(World world, Creature player){
    	screenWidth = 80;
        screenHeight = 20;
    	this.world = world;
    	this.player = this.world.getPlayer(player.x, player.y);
    }
    
    /**
     * Metodi joka tuo kent�lle pelaajan ja omenat
     * @param creatureFactory
     */
    private void createCreatures(CreatureFactory creatureFactory){
    	player = creatureFactory.newPlayer();
    	
    	for(int i = 0; i < 8; i++){
    		creatureFactory.newApple();
    	}
    }
    
    /**
     * metodi maailman luomiseksi
     */
    private void createWorld(){
        world = new WorldBuilder(80, 20).makeWorld().build();
    }
    
    /**
     * kertoo scrollauksen x-akselilla
     * @return scrollin (pelaajan) sijainti x-akselilla
     */
    public int getScrollX() {
        return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth));
    }
    
    /**
     * kertoo scrollauksen y-akselilla
     * @return scrollin (pelaajan) sijainti y-akselilla
     */
    public int getScrollY() {
        return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight));
    }
    
    
    /**
     * metodi joka n�ytt�� kent�ll� olevat ruudut
     * @param terminal
     * @param left
     * @param top
     */
    private void displayTiles(AsciiPanel terminal, int left, int top) {
        for (int x = 0; x < screenWidth; x++){
            for (int y = 0; y < screenHeight; y++){
                int wx = x + left;
                int wy = y + top;
     
                Creature creature = world.creature(wx, wy);
                if (creature != null)
                    terminal.write(creature.glyph(), creature.x - left, creature.y - top, creature.color());
                else
                    terminal.write(world.glyph(wx, wy), x, y, world.color(wx, wy));
            }
        }
    }

    
    /**
     * kertoo peli-ikkunalle mit� tulee tulostaa
     */
	@Override
	public void displayOutput(AsciiPanel terminal) {
		int left = getScrollX();
        int top = getScrollY();
    
        displayTiles(terminal, left, top);
        terminal.writeCenter("-- Paina [Esc] tallentaaksesi pelin --", 21);
        if(saved != null){
        	terminal.writeCenter(saved, 22);
        }
		terminal.writeCenter("-- Ker"+(char)132+(char)132+" kaikki herkulliset omenat koriisi, mutta varo pahoja omenoita! --", 23);
		terminal.write("Omenoita: " + world.score, 1, 20);
	}
	
	
	public void saveGame(){
		Serializer serializer = new Serializer();
		serializer.serializeWorld(world);
		serializer.serializeCreature(player);
		saved = "Tallennettu!";
	}
	
	/**
	 * metodi joka vastaa pelaajan painalluksiin
	 * metodi sis�lt�� my�s mahdollisuuden ker�t� m�t� omena, joka on 10%
	 * t�m�n sattuessa heitet��n h�vi�misruutu
	 */
	@Override
	public Screen respondToUserInput(KeyEvent key) {
		int a = world.score;
		switch (key.getKeyCode()){
		case KeyEvent.VK_ESCAPE: saveGame(); break;
		case KeyEvent.VK_LEFT:
        case KeyEvent.VK_H: player.moveBy(-1, 0); break;
        case KeyEvent.VK_RIGHT:
        case KeyEvent.VK_L: player.moveBy( 1, 0); break;
        case KeyEvent.VK_UP:
        case KeyEvent.VK_K: player.moveBy( 0,-1); break;
        case KeyEvent.VK_DOWN:
        case KeyEvent.VK_J: player.moveBy( 0, 1); break;
        case KeyEvent.VK_Y: player.moveBy(-1,-1); break;
        case KeyEvent.VK_U: player.moveBy( 1,-1); break;
        case KeyEvent.VK_B: player.moveBy(-1, 1); break;
        case KeyEvent.VK_N: player.moveBy( 1, 1); break;
		}
		if (a < world.score){
			double b = Math.random();
			if(b < 0.1){return new LoseScreen();}
		}
		if(world.score == 8){return new WinScreen();}
		
		return this;
	}
	
	
	
}
