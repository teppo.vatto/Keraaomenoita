package KeraaOmenoitaPeli;

import javax.swing.JFrame;
import asciiPanel.AsciiPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import KeraaOmenoitaPeli.screens.Screen;
import KeraaOmenoitaPeli.screens.StartScreen;
 
public class ApplicationMain extends JFrame implements KeyListener {
	//sarjanumero
	private static final long serialVersionUID = 1060623638149583738L;
	//ATTRIBUUTIT
	private AsciiPanel terminal;
	private Screen screen;
	
	//KONSTRUKTORI
	public ApplicationMain(){
		super();
		terminal = new AsciiPanel();
		add(terminal);
		pack();
		screen = new StartScreen();
		addKeyListener(this);
		repaint();
	}
	
	/**
	 * metodi joka maalaa jatkuvasti ruudulle outputin
	 */
	@Override
	public void repaint(){
		terminal.clear();
		screen.displayOutput(terminal);
		super.repaint();
	}
	
	/**
	 * metodi joka vastaa napin painalluksiin
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		screen = screen.respondToUserInput(e);
		repaint();
	}
	
	/**
	 * napin vapautus
	 */
	@Override
	public void keyReleased(KeyEvent e) { }
	
	/**
	 * napin painallus
	 */
	@Override
	public void keyTyped(KeyEvent e) { }
	
	/**
	 * main metodi
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationMain app = new ApplicationMain();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.setVisible(true);
	}
}
